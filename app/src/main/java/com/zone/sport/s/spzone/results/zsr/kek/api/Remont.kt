package com.zone.sport.s.spzone.results.zsr.kek.api

data class Remont (
    var id: String,
    var date: String,
    var time: Long,
    var team1: String,
    var team2: String,
    var league: String,
    var res1: String,
    var res2: String,
    var endTime: Long,
    var isLive: Boolean,
    var logo1: String,
    var logo2: String
){
    fun getSFFqqw(): String {
        val s = "$id $date $time $team1"
        return s + "\n" + s
    }
}