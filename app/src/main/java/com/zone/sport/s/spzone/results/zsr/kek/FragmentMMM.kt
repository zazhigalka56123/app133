package com.zone.sport.s.spzone.results.zsr.kek

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.button.MaterialButton
import com.zone.sport.s.spzone.results.zsr.Crocodile
import com.zone.sport.s.spzone.results.zsr.R
import com.zone.sport.s.spzone.results.zsr.databinding.FragmentMmmBinding
import com.zone.sport.s.spzone.results.zsr.db.H
import com.zone.sport.s.spzone.results.zsr.db.RepositoryImpl
import com.zone.sport.s.spzone.results.zsr.gdfgdfg.BSDF
import com.zone.sport.s.spzone.results.zsr.kek.api.Remont
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Random

class FragmentMMM: Fragment() {

    var flag = true
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val b = FragmentMmmBinding.inflate(inflater, container, false)

        lateinit var remont: Remont
        with(Crocodile.remont) {
            if (this == null){
                Toast.makeText(requireContext(), "Произошла ошибка", Toast.LENGTH_SHORT).show()
                findNavController().popBackStack()
            }else{
                remont = this
            }
        }

        if (remont.endTime < System.currentTimeMillis()/ 1000 && !remont.isLive){
            b.fewrw.text = "Матч завершён"
            b.fewrw.isClickable = false
            flag = false
            println("!!!time end ${remont.endTime}")
            println("!!!time start ${remont.time}")
            println("!!!time curr ${System.currentTimeMillis()}")
        }else{
            b.fewrw.text = "Сделать ставку"
        }
        lifecycleScope.launch(Dispatchers.Main) {
            val repository = RepositoryImpl(requireActivity().application)
            val list = repository.getFAs()
            var flag = false
            list.forEach {
                if (it.id == remont.id){
                    flag = true
                }
            }
            if (flag) {
                withContext(Dispatchers.Main){
                    b.fewrw.text = "Ставка сделана"
                    b.fewrw.isClickable = false
                }
            }
        }


        with(b){
            trulalala.setOnClickListener { findNavController().popBackStack() }
            fdsfsdffdsfs.text = remont.team1
            tewtw.text = remont.team2

            popo.text = remont.league


            this.cvwrqw.setCardBackgroundColor(Color.parseColor(remont.logo1))
            this.cardView2.setCardBackgroundColor(Color.parseColor(remont.logo2))


            erwerw.text = "${remont.res1} : ${remont.res2}"

            val p1: Int = (Random().nextInt(50) + 20)
            val p2 = Random().nextInt(20)
            rwerwe.text = "1.$p1"
            rewrwe.text = "2.$p2"

            var p = p1



            fewrw.setOnClickListener {
                if (flag) {
                    val builder = AlertDialog.Builder(requireContext())

                    val customLayout: View = layoutInflater.inflate(R.layout.fragment_stavka, null)
                    builder.setView(customLayout)
                    val dialog = builder.create()

                    val eter = customLayout.findViewById<EditText>(R.id.etet)
                    val win = Random().nextInt(2) == 1

                    customLayout.findViewById<MaterialButton>(R.id.btn_go_next).setOnClickListener {
                        if (eter.text.isNotEmpty()) {
                            val st = eter.text.toString().toInt()
                            if (st > 0 && st < BSDF(requireActivity()).ffsdfdsfsbala()) {
                                lifecycleScope.launch(Dispatchers.IO) {
                                    val repository = RepositoryImpl(requireActivity().application)
                                    repository.add(
                                        H(
                                            id = remont.id,
                                            team1 = remont.team1,
                                            team2 = remont.team2,
                                            res1 = remont.res1,
                                            res2 = remont.res2,
                                            logo1 = remont.logo1,
                                            logo2 = remont.logo2,
                                            date = remont.date,
                                            time = remont.time.toLong(),
                                            endTime = remont.endTime,
                                            stavka = st,
                                            koef = 100 + p,
                                            teamM = if (win && p == p1) 1 else 2,
                                            win = win,
                                            state = "not done"
                                        )
                                    )
                                    withContext(Dispatchers.Main) {
                                        BSDF(requireActivity()).gsdgsdgdsgkmkgmndn(
                                            BSDF(
                                                requireActivity()
                                            ).ffsdfdsfsbala() - st
                                        )
                                        BSDF(requireActivity()).game(
                                            BSDF(
                                                requireActivity()
                                            ).gewtwetfdsf() + 1
                                        )
                                        Toast.makeText(
                                            requireContext(),
                                            "Ставка сделана!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        b.fewrw.text = "Ставка сделана"
                                        b.fewrw.isClickable = false
                                    }
                                    dialog.cancel()
                                }
                            } else {
                                Toast.makeText(
                                    requireContext(),
                                    "Не хватает денег",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                    customLayout.findViewById<ImageView>(R.id.close).setOnClickListener {
                        dialog.cancel()
                    }
                    customLayout.findViewById<ImageView>(R.id.close)

                    customLayout.findViewById<MaterialButton>(R.id.eqwe1).setOnClickListener {
                        customLayout.findViewById<MaterialButton>(R.id.eqwe1).background.setTint(
                            requireContext().getColor(R.color.gr)
                        )
                        customLayout.findViewById<MaterialButton>(R.id.eqwe2).background.setTint(
                            requireContext().getColor(R.color.gray)
                        )
                        p = p1
                    }

                    customLayout.findViewById<MaterialButton>(R.id.eqwe2).setOnClickListener {
                        customLayout.findViewById<MaterialButton>(R.id.eqwe1).background.setTint(
                            requireContext().getColor(R.color.gray)
                        )
                        customLayout.findViewById<MaterialButton>(R.id.eqwe2).background.setTint(
                            requireContext().getColor(R.color.gr)
                        )
                        p = p2
                    }
                    dialog.show()
                }else{
                Toast.makeText(requireContext(), "Матч завершён. Ставки сделать нельзя", Toast.LENGTH_SHORT).show()
            }
        }


        }




        return b.root
    }
}