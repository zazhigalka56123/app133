package com.zone.sport.s.spzone.results.zsr.kek

import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.zone.sport.s.spzone.results.zsr.Crocodile
import com.zone.sport.s.spzone.results.zsr.R
import com.zone.sport.s.spzone.results.zsr.kek.api.Remont

import com.zone.sport.s.spzone.results.zsr.kek.api.GgGq
import com.zone.sport.s.spzone.results.zsr.databinding.FragmentMBinding
import com.zone.sport.s.spzone.results.zsr.gdfgdfg.BSDF
import com.zone.sport.s.spzone.results.zsr.kek.domiki.Qremniy
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Calendar

class FragmentM: Fragment() {
    val list: MutableLiveData<List<Remont>> = MutableLiveData(listOf())
    val list1: MutableLiveData<List<Remont>> = MutableLiveData(listOf())
    val list2: MutableLiveData<List<Remont>> = MutableLiveData(listOf())
    val list3: MutableLiveData<List<Remont>> = MutableLiveData(listOf())
    val list4: MutableLiveData<List<Remont>> = MutableLiveData(listOf())

    var done1 = false
    var done2 = false
    var done3 = false
    var done4 = false
    var done5 = false

    val t1 = "Сейчас нет информации, попробуйте позже"
    val t2 = "Проверьте интернет соединение"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val b = FragmentMBinding.inflate(inflater, container, false)
        val rep = GgGq(requireContext(), "https://allsportsapi2.p.rapidapi.com/")

        b.materialTextView54.text = BSDF(requireActivity()).ffsdfdsfsbala().toString()  + " P"
        b.etGames.text = BSDF(requireActivity()).gewtwetfdsf().toString()

        b.history.setOnClickListener {
            findNavController().navigate(R.id.fragment3)
        }

        val date = DateFormat.format("dd-MM-yyyy", Calendar.getInstance().time).toString()
        val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH).toString()
        val month = (Calendar.getInstance().get(Calendar.MONTH) + 1).toString()
        val year = Calendar.getInstance().get(Calendar.YEAR).toString()

        lifecycleScope.launch(Dispatchers.IO) {
            val l2 = mutableListOf<Remont>()
            val response = rep.oipiqw("Livescore", day, month, year, 0)
            Log.d("rewrew", response.toString())
            if (response != null) {
                response.forEach { it.date = date }
                l2.addAll(response)
                list.postValue(l2)
                done1 = true
            }else{
                withContext(Dispatchers.Main){
                    done1 = true
                    b.visdfq.visibility = View.GONE
                    b.t1.text = t2
                    b.t1.visibility = View.VISIBLE
                }
            }
            val l = mutableListOf(
                mutableListOf<Remont>(),
                mutableListOf<Remont>(),
                mutableListOf<Remont>(),
                mutableListOf<Remont>(),
                )
            for (i in 0..3) {
                val r = rep.oipiqw("Fixture", day, month, year, i)
                if (r != null) {
                    r.forEach { it.date = date }
                    l[i].addAll(r)
                    when (i) {
                        0 -> {
                            list1.postValue(l[i])
                            done2 = true
                        }
                        1 -> {
                            list2.postValue(l[i])
                            done3 = true
                        }
                        2 -> {
                            list3.postValue(l[i])
                            done4 = true
                        }
                        else -> {
                            list4.postValue(l[i])
                            done5 = true
                        }
                    }
                }else {
                    withContext(Dispatchers.Main) {
                        when (i) {
                            0 -> {
                                done2 = true
                                b.fdfsdf2.visibility = View.GONE
                                b.t2.text = t2
                                b.t2.visibility = View.VISIBLE
                            }

                            1 -> {
                                done3 = true
                                b.fdfsdf3.visibility = View.GONE
                                b.t3.text = t2
                                b.t3.visibility = View.VISIBLE
                            }

                            2 -> {
                                done4 = true
                                b.fdfsdf4.visibility = View.GONE
                                b.t4.text = t2
                                b.t4.visibility = View.VISIBLE
                            }

                            else -> {
                                done5 = true
                                b.fdfsdf5.visibility = View.GONE
                                b.t5.text = t2
                                b.t5.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            }
        }

        val a = Qremniy()
        with(b.rvyuioe){
            adapter = a
            val l = LinearLayoutManager(requireContext())
            l.orientation = LinearLayoutManager.HORIZONTAL
            layoutManager = l
        }
        a.click = {
            Crocodile.remont = it
            findNavController().navigate(R.id.fragment2)
        }

        list.observeForever {
            a.submitList(it)
            if (it.isNotEmpty() && done1){
                b.visdfq.visibility = View.GONE
                b.t1.visibility = View.GONE
            }else if(done1){
                b.visdfq.visibility = View.GONE
                b.t1.text = t1
                b.t1.visibility = View.VISIBLE
            }
        }

        val a1 = Qremniy()
        with(b.recyclerView2){
            adapter = a1
            val l = LinearLayoutManager(requireContext())
            l.orientation = LinearLayoutManager.HORIZONTAL
            layoutManager = l
        }
        a1.click = {
            Crocodile.remont = it
            findNavController().navigate(R.id.fragment2)
        }

        list1.observeForever {
            Log.d("fsdfsd", it.toString())
            a1.submitList(it)
            if (it.isNotEmpty() && done2){
                b.fdfsdf2.visibility = View.GONE
                b.t2.visibility = View.GONE
            }else if(done2){
                b.t2.text = t1
                b.fdfsdf2.visibility = View.GONE
                b.t2.visibility = View.VISIBLE
            }
        }

        val a2 = Qremniy()
        with(b.recyclerView3){
            adapter = a2
            val l = LinearLayoutManager(requireContext())
            l.orientation = LinearLayoutManager.HORIZONTAL
            layoutManager = l
        }
        a2.click = {
            Crocodile.remont = it
            findNavController().navigate(R.id.fragment2)
        }

        list2.observeForever {
            a2.submitList(it)
            if (it.isNotEmpty() && done3){
                b.fdfsdf3.visibility = View.GONE
                b.t3.visibility = View.GONE
            }else if(done3){
                b.t3.text = t1
                b.fdfsdf3.visibility = View.GONE
                b.t3.visibility = View.VISIBLE
            }
        }
        val a3 = Qremniy()
        with(b.recyclerView4){
            adapter = a3
            val l = LinearLayoutManager(requireContext())
            l.orientation = LinearLayoutManager.HORIZONTAL
            layoutManager = l
        }
        a3.click = {
            Crocodile.remont = it
            findNavController().navigate(R.id.fragment2)
        }
        list3.observeForever {
            a3.submitList(it)
            if (it.isNotEmpty() && done4){
                b.fdfsdf4.visibility = View.GONE
                b.t4.visibility = View.GONE
            }else if(done4){
                b.t4.text = t1
                b.fdfsdf4.visibility = View.GONE
                b.t4.visibility = View.VISIBLE
            }
        }
        val a4 = Qremniy()
        with(b.recyclerView5){
            adapter = a4
            val l = LinearLayoutManager(requireContext())
            l.orientation = LinearLayoutManager.HORIZONTAL
            layoutManager = l
        }

        a4.click = {
            Crocodile.remont = it
            findNavController().navigate(R.id.fragment2)
        }

        list4.observeForever {
            a4.submitList(it)
            if (it.isNotEmpty() && done5){
                b.fdfsdf5.visibility = View.GONE
                b.t5.visibility = View.GONE
            }else if(done5){
                b.t5.text = t1
                b.fdfsdf5.visibility = View.GONE
                b.t5.visibility = View.VISIBLE
            }
        }



        return b.root
    }
}