package com.zone.sport.s.spzone.results.zsr.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "matches")
data class H (
    @PrimaryKey
    var id: String = "",
    var team1: String,
    var team2: String,
    var res1: String,
    var res2: String,
    var logo1: String,
    var logo2: String,
    var date: String,
    var time: Long,
    var stavka: Int,
    var koef: Int,
    var endTime: Long,
    var teamM: Int,
    var win: Boolean,
    var state: String
)