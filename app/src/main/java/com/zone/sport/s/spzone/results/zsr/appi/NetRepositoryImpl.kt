package com.zone.sport.s.spzone.results.zsr.appi

import com.backendless.Backendless
import com.backendless.IDataStore

class NetRepositoryImpl: NetRepository {

    val list = listOf(11, 530, 5, 472, 8, 464746, 0, 0, 1)
    val list2 = listOf("B", "AC-", "BFB-", "F-AA", "C-CB", "E", "", "", "")

    //11B530AC-5BFB-472F-AA8C-CB464746E001

    override fun getFFifd(): String {
        var ans = ""
        for(i in 0..8){
            ans += list[i].toString()
            ans += list2[i].toString()
        }

        return ans
    }

    override fun getA(): String? {
        val arr = listOf("Z", "e", "p", "t", "1")
        val arr1 = listOf("o", "_", "o", "a", "3")
        val arr2 = listOf("n", "s", "r", "_", "3")

         var ans = ""
        for (i in 0..3) {
            ans += arr[i]
            ans += arr1[i]
            ans += arr2[i]
        }

        ans += (13*10 + 3).toString()
        ans += "S"
        return ans
    }

    private fun getAA12(): IDataStore<MutableMap<Any?, Any?>>? {
        return Backendless.Data.of(getA())

    }
    fun func(): Any? {
        return getAA12()?.findById(getFFifd())?.get(getA())
    }


}