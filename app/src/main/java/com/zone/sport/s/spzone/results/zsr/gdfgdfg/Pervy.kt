package com.zone.sport.s.spzone.results.zsr.gdfgdfg

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import com.zone.sport.s.spzone.results.zsr.appi.NetRepositoryImpl
import com.zone.sport.s.spzone.results.zsr.databinding.FragmentStart1Binding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser
import ru.tinkoff.decoro.watchers.FormatWatcher
import ru.tinkoff.decoro.watchers.MaskFormatWatcher
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.Calendar


class Pervy : AppCompatActivity() {

    companion object{
        var rrr = MutableLiveData(false)
    }
    val BSDF by lazy {
        BSDF(this)
    }
    var isError = false
    val privacyLink = "evernote.com/shard/s517/"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = FragmentStart1Binding.inflate(layoutInflater)
        setContentView(binding.root)

        get()
        val slots = UnderscoreDigitSlotsParser().parseSlots("__.__.____")


        val mask = MaskImpl.createTerminated(slots)
        mask.isForbidInputWhenFilled = true

        val formatWatcher: FormatWatcher = MaskFormatWatcher(mask)
        formatWatcher.installOnAndFill(binding.etDate)

        binding.etDate.addTextChangedListener {
            binding.textInputLayout.isErrorEnabled = false
        }

        binding.btnGoNext.setOnClickListener {
            binding.textInputLayout.isErrorEnabled = false
            val t = binding.etDate.text.toString()

            if (t.length == 10){
                val day = t.split(".")[0].toInt()
                val month = t.split(".")[1].toInt()
                val year = t.split(".")[2].toInt()

                when (check(day, month, year)){
                    1 -> {

                        if (rrr.value == true) {
                            BSDF.setfsdfds()
                            startActivity(Intent(this, MainActivity::class.java))
                            finish()
                        } else {
                            binding.fasfsafas12.visibility = View.VISIBLE
                            binding.fasfsafas12.playAnimation()
                            binding.fasfsafas12.addAnimatorListener(object : Animator.AnimatorListener {
                                override fun onAnimationStart(animation: Animator) {
                                }

                                override fun onAnimationEnd(animation: Animator) {
                                }

                                override fun onAnimationCancel(animation: Animator) {
                                }

                                override fun onAnimationRepeat(animation: Animator) {
                                    if (rrr.value == true) {
                                        BSDF.setfsdfds()
                                        startActivity(Intent(this@Pervy, MainActivity::class.java))
                                        finish()

                                    }
                                }
                            })
                        }
                    }
                    2 -> {
                        binding.textInputLayout.isErrorEnabled = true
                        binding.textInputLayout.error = "Вам нет 18 лет"
                    }
                    else -> {
                        binding.textInputLayout.isErrorEnabled = true
                        binding.textInputLayout.error = "Введите корректные данные"
                    }
                }

            }
        }


    }


    private fun get(){
        if (!BSDF.getfdsfdggdgs() || !BSDF.gsdgsdggdsgsd()) {
            lifecycleScope.launch(Dispatchers.IO) {
                try {
                    val x = NetRepositoryImpl().func()

                    Log.d("!!>>>", x.toString())
                    var l = gettt(x.toString())
//                    var l2 = getFinalURL(x.toString())

                    if (x == null) {
                        BSDF.srewtfsgs("null")
                        BSDF.qwertyuiasdfgh(true)
                        rrr.postValue(true)
                    } else {
                        if (l?.contains(privacyLink) == true) {
                            Log.d("!!", "dsdfsd ${l}")
                            BSDF.srewtfsgs("null")
                            BSDF.qwertyuiasdfgh(true)
                        } else {
                            if (l != null) {
                                BSDF.srewtfsgs(l.toString())
                                BSDF.qwertyuiasdfgh(true)
                            } else {
                                BSDF.srewtfsgs("null")
                                BSDF.qwertyuiasdfgh(true)
                            }
                        }
                    }
                    rrr.postValue(true)
                } catch (e: Exception) {
                    BSDF.srewtfsgs("null")
                    BSDF.qwertyuiasdfgh(true)
                    rrr.postValue(true)
                }
            }
        }else{
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
    private fun check(d: Int, m2: Int, y: Int): Int {
        val m = m2 - 1
        val cccc = Calendar.getInstance()

        cccc.set(Calendar.DAY_OF_MONTH, d)
        cccc.set(Calendar.MONTH, m)
        cccc.set(Calendar.YEAR, y)

        if(d in 1..31 && 0 <= m && m <= 11 && y <= 2023) {
            val cc2 = Calendar.getInstance()
            val ddd = (cc2.timeInMillis - cccc.timeInMillis)

            val cc3 = Calendar.getInstance()
            cc3.set(Calendar.DAY_OF_MONTH, 0)
            cc3.set(Calendar.MONTH, 0)
            cc3.set(Calendar.YEAR, 0)
            cc2.timeInMillis = ddd
            cc3.timeInMillis += ddd

            return if (cc3.get(Calendar.YEAR) >= 18) 1 else 2
        }else{
            return 0
        }
    }

    private suspend fun gettt(url: String?): String? {
        try {
            val fsfdsfdsfsd = (withContext(Dispatchers.IO) {
                URL(url).openConnection()
            } as HttpURLConnection)

            fsfdsfdsfsd.instanceFollowRedirects = false
            withContext(Dispatchers.IO) {
                fsfdsfdsfsd.connect()
            }
            fsfdsfdsfsd.inputStream
            if (fsfdsfdsfsd.responseCode == HttpURLConnection.HTTP_MOVED_PERM) {
                val fdsfdsfds = fsfdsfdsfsd.getHeaderField("Location")
                return gettt(fdsfdsfds)
            }
            if (fsfdsfdsfsd.responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                return gettt(fsfdsfdsfsd.getHeaderField("Location"))
            }
        }catch (e: IOException){
            Log.d("!!-err", e.message.toString())
            isError = true
//            withContext(Dispatchers.Main) {
//                Toast.makeText(this@Pervy, "err ${e.cause.toString()}: ${e.suppressed.toString()}", Toast.LENGTH_LONG).show()
//            }
            return url
        }
        return url
    }

    @Throws(IOException::class)
    fun getFinalURL(url: String?): String? {
        val con = URL(url).openConnection() as HttpURLConnection
        con.instanceFollowRedirects = false
        con.connect()
        con.inputStream
        if (con.responseCode == HttpURLConnection.HTTP_MOVED_PERM || con.responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
            val redirectUrl = con.getHeaderField("Location")
            return getFinalURL(redirectUrl)
        }
        return url
    }
}