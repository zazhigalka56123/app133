package com.zone.sport.s.spzone.results.zsr.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TaleDao {

    @Query("SELECT * FROM matches")
    suspend fun getaFas(): List<H>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(taleDbModel: H)
//    {
//        var s = ""
//        taleDbModel.team1.forEach {
//            s += it
//        }
//    }

}